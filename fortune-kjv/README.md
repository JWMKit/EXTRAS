## Fortune KJV Verse

**Purpose**: A  fortune cookie database of King James Bible verses

**Dependencies**:  fortune  

**Recommend Usage** : use with the [yad_fortune_verse](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/yad_fortune_verse)  script found in this repository for a bible verse pop up at login.


A total of  1481 Verses.  More than four years at a verse a day.

| File  | Description |
|--|--|
|fortune-kjv-verse  | The verses |
| fortune-kjv-verse.dat | index file  |

**INFO** :  Some fortune programs require the index file.  For proper functionality both files should be stored in the same file path.
