## ALSA Report

Adjust the ALSA Master level while providing the user visual feedback .

NOTE : If you need sndio support look at [sndio_report](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/sndio-report) 

### Dependencies
 alsa_utils - audio control  
 yad - notification

### Purpose

Keyboard shortcuts linked to alsa_report and can adjust the alsa master by turning the volume up and down and toggling mute. A notification of the current level is provided in percentage, and Mute and Unmute are clearly reported.

### Basic Instructions

* Give the  file permission to execute  
* Assign keyboard shortcuts using the chart below.  
* JWMKit Users can use jwmkit_keys to configure the shortcuts.

### Command Chart

| Action | Command |
|--|--|
| Increase Volume | alsa_report +  |
|  Decrease Volume | alsa_report - |
|  Mute / Unmute | alsa_report m |

**IMPORTANT**: This chart does not use the full path for the file alsa_report.  You may need to change it to the full path.  Example: /home/user/alsa_report

### Easy modifications.
The Defaults may not be optimal for some configuration. Here are some useful tips.

**Notification position**  

Default  :  35 pixels away from the bottom/left edge of the screen.  
Modify  : Change the value of x and y at the top of the script using positive and negative integers. (see chart below) 

|  | Positive number | Negative number |
|--|--|--|
| **x** | Distance from the Left edge  | Distance from the Right edge |
| **y** | Distance from the Top edge  | Distance from the Bottom edge |

 **Mute Message**  
 
Default : appropriate value of either "Mute"  or "Unmute"
Modify : search the script for the following lines.  

	then a="Unmute"
	a="Mute"

Change the values Mute and Unmute to desired message. Example:

	then a="ON"
	a="OFF"

