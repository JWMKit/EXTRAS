## JWMKit Easy Menu Bash Script

A simpler version of JWMKit Easy Menu .

Advantages

- Works without JWMKit
- No dependencies
- No need to install anything.
-  It's a simple script. Just give it permission to run
- limits apps to a single category.  (prevents multiple listing of an app)

Disadvantages (compared to the full version)

- less features
- less control

### UPDATES

**December 21, 2023**  

The previous version was a "quick and dirty" release. This is a much better version.

- Code clean up
- User specific freedesktop files are now used
- fail-safe for improperly name freedesktop files
- fail safe for freedesktop file with missing/corrupt data
- Script is easier to modify for user's preferred category names, and icons.

### USAGE

**Step 1** :  Give the script permission to execute.  

Either use your file manager or from the command line.

	chmod +x jwmkit_easymenu

**Step 2** : Choose your terminal.   

You will find a section labeled "User Customization Variables" at  top part of the script directly under the license declaration.  It contains an area titled XTERM.  

You should will see the following line.

	XTERM=xterm

Change the value to the command that executes your preferred terminal.

Example:

	XTERM=xfce4-terminal

**Step 3** : Edit your JWM configuration 

To use Easy menu add it as an include into a rootmenu. 

 Example:
 
```
<JWM>   
   <RootMenu onroot="3"> 
      <Include>exec:jwmkit_easymenu</Include>
   </RootMenu>   
</JWM>
```

Things to remember 

- Replace the exec: line with the full path of where the file is stored.  
- Don't forget the "exec:" part!

### Easy Modifications.

Some effort has been made to make this script easy to modify. The steps above already covered assigning your preferred terminal.  

You can also easily modify the Name and icons of the Categories. Just using the provide Cat_Names, and Cat_Icons variables arrays at the top of the script.

Instructions are provided in the comments of the script.

### Simple (yet more advanced) Modifications

**Remove a category from your menu.**  

Let's assume the settings menu is redundant because you use a settings manager that shows the same items.

To remove the settings category from the menu find this line

	if [[ ${#Settings[@]} != 0 ]]; then

Then change the comparison to itself. This way the condition is always false.

	if [[ 0 != 0 ]]; then


NOTE : This will work for any category since it just forces the script to skip the next section of code. Settings makes a good example due to the use case mention.

It's also common to hide Science, Education, and Development categories.  I would advise editing the freedesktops to ensure those items still appear in another menu.
