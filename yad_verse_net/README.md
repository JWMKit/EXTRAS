## yad\_verse\_net - Verse of the Day Splash Screen

**Purpose**:  Grab the Verse of the Day from biblegateway.com and display a splash screen with the "verse of the day" .  

**Requires** : Internet

**Dependencies**:  wget  , yad  

**Recommend Usage**: Add to system start up so the user will see the "verse of the day" after login

**Parameters**
Translation : Default is KJV.  Follow the command with the abrivation of the prefered translation. 

Examples

	yad_verse_net NIV
	yad_verse_net ASV
	yad_verse_net MSG

**JWM Kit Basic Instructions**

* Give the yad\_verse\_net file permission to execute  
* Use JWM Kit Startup to add it to start up.  
