#!/usr/bin/python3
import os, re

expath = "$HOME/.config/jwmkit/extras/audiotoggle"
data = os.popen('amixer').read() + "\nsdf"
data = re.findall('(imple mixer.+?\n[^ ])', data, re.S)
dlist = []
for d in data:
    d = re.findall("imple mixer[^']+'([^']+)'.+?(Playback|Capture).+?%.+?(on|off)", d, re.S)
    dlist.extend(d)
output ="<JWM>\n<Separator />\n"
output = "{}<Program label=\"Enable/Disable\" />\n<Separator />\n".format(output)

for d in dlist:
    if "f" in d[2]:
        stat = "on"
    else:
        stat = "off"
    output = '{}\n<Program label="[{}] - {}" >{} "{}" {}</Program>'.format(output, d[2], d[0], expath, d[0], stat)
output = "{}</JWM>".format(output)
print(output)
