# Amixer Toggle Menu for JWM

A dynamic menu for JWM that list amixer audio controls and allows you to view and toggle the On or Off status of each control listed.

Depends on Python3, amixer, yad, and of course JWM

## Operation
The menu pops up when triggered with a keyboard shortcut or mouse action binded to a traybutton.  Clicking on an entry in the menu will enable or disable the selected control. (Reversing the current state)

## Preview
See the file amixmenu.png for a preview.  

Here is a text based representation of the layout.

Note the controls listed in the menu are specific to your machine's audio hardware.

***
&ensp;Enable/Disable
***
[on]  - Master  
[on]  - PCM   
[off]  - Video   
[off]  - Line   
[off]  - Capture   

***

## Configuration

**Path** - Where to place the files

NOTE : The paths used in this configuration can be change, but will require altering the paths in the code.

Use chart below to download the files and save them to the recommended paths.

| File | purpose | path |
|--|--|--|
| [amix](https://codeberg.org/JWMKit/EXTRAS/raw/branch/main/Amix_Menu/amix)  | jwm xml | $HOME/.config/jwm/ |
| [amixmu.py](https://codeberg.org/JWMKit/EXTRAS/raw/branch/main/Amix_Menu/amixmu.py) | generates the menu | $HOME/.config/jwm/ extras/  |
| [audiotoggle](https://codeberg.org/JWMKit/EXTRAS/raw/branch/main/Amix_Menu/audiotoggle)  | (dis)enable selected control | $HOME/.config/jwm/ extras/   |

**Executable Permission**

The files amixmu.py and  audiotoggle require permission to run.  Most file managers will assist you in setting the permission of the files.  You can use the command line.  Example command (with full path)

	chmod +x ~/.config/jwm/ extras/amixmu.py ~/.config/jwm/ extras/audiotoggle
	

**JWM config**

JWM configuration consist of 2 parts.  Included the amix XML file in your JWM config, and Bind the menu to an traybutton, mousebutton, or a Keyboard shortcut.

***Including the amix XML***

If you place the file in the path recommended above JWMKit Menus should find the files.  

JWMKit Instructions to include the amix xml file  

-  Open jwmkit_menus  
- Click the "Create/Import/Restore" button.  You can find it next to the about button in the top left.  
- Select the Restore Tab  
- Select the amix file from the list.   (note the full path will be given)
- Assign the onroot value.  I used "A" (a for audio), but use any letter you wish, or a number for a mouse button without an traybutton icon.  
- Click Apply.  
 
TIP! -   If you did not see the amix file in the restore tab either ensure it's in the correct path, or you can use the import tab to find it.  



***Bind the menu*** to a Traybutton, or/and a keyboard shortcut.  

Tips!   

-  You don't have to do both steps unless you just want to. Pick either the Traybutton, or Keyboard step and skip the one you do not need.  
-  You can skip both steps below if you set the onroot value to 1, 2, or 3. You should already be able to access the menu by  clicking the assigned mouse button on the desktop.

***Traybutton*** (A icon on the tray)   
Open jwmkit_trays and select the tray you wish to add the menu to, and then either add a new traybutton, or select an exsiting one to bind the menu too. I recommend using the exsiting volume icon and using a free (or duplicated) mouse button.  Set value in the entry next to desired mousebutton(s)  to root:A (unless you used a different on root value then replace "A" with the value you used.

***Keyboard shortcut***  
use jwmkit_keys to create a desire shortcut and set the action to root:a   (or as explained above to the value you configure the menu's onroot value too)


**DONE**
If you've done it correctly you should now have a new menu that allows you to view and toogle the status of various audio controls.  

**Trouble shooter**  

If the menu does not work Try these suggestions.  

- Restart JWM
- Double check your configuration.  Ensure the value for tirggering the menu is correct.
- Verify that YAD installed.
- Verify that amixer installed
