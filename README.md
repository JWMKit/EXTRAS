## JWM Kit's Extras
A repository for scripts and data useful with various windows managers, including JWM and/or JWM Kit

| Directory | Purpose |
|--|--|
| app_toggle |Toggle the execution of an app |
| alsa-report  | alsa control and splash notification |
|Amix_Menu  | JWM Dynamic Menu for audio control  |
| fortune-kjv | KJV fortune cookie datebase |
| JWMKit_EasyMenu | EasyMenu without JWMKit |
| jwmkit_easymenu_cpp  |  EasyMenu C++ version - no depends |
| JWMKit_EasyMenu_Bash |  EasyMenu simplified with no depends |
| sndio-report  | sndio control and splash notification |
| Tray_Toggle | toggle the visablity of a JWM Tray |
| VerseOfDay | display the output of the verse command |
| yad_fortune | splash screen for the fortune command |
|  yad_fortune_verse | splash screen with fortune function  |
| yad_verse_net | Bible verse splash screen requiring Internet   |
| X_of_the_Day | Daily Message Splash  retrieved from a datafile  |


