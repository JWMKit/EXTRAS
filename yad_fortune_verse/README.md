## yad_fortune_verse - Splash screen with 'fortune' function

**April 24, 2023** Bugfix. The script was missing the  command to change the current work directory when using the "default database"

**Purpose**:  Display a splash screen with the with a  random message.  

**Difference from yad_fortune?** :  

* Provides the "fortune" functionality.  Fortune is not required as a dependency. 
* Defaults to the fortune-kjv database if it is in the same directory and no database is specified
* Will not find "installed fortunes"  You must provide the path for the fortune file.

**Dependencies**:  yad  

**Recommend Usage**

 -  Add to system start up so the user will see a message at login
 - use with the [fortune-kjv](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/fortune-kjv)  database found in this repository for a bible verse pop up at login.

**Usage / Commandline**  
you can specify a fortune cookie database as an command line argument.  

When run with no argument the splash displays the output of the command "fortune".  

Syntax Examples  :  
NOTE : Examples for JWM startup. In the terminal you need to add ./ or the full path before "yad\_fortune\_verse"

Use the fortune database "fortune-kjv" saved in your home folder.  

	yad_fortune_verse "$HOME/fortune-kjv"

Use the  "fortune-kjv"  database when save in the same directory as this script

	yad_fortune_verse


**JWM Kit Basic Instructions**

* Give the yad\_fortune\_verse file permission to execute  
* Use JWM Kit Startup to add it to start up.  