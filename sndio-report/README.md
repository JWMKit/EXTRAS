## SNDIO Report

Adjust the SNDIO Master level while providing the user visual feedback .

NOTE : If you need alsa support look at [alsa_report](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/alsa-report) 

### Dependencies
 sndio - audio control  
 yad - notification

### Purpose

A keyboard shortcut linked to sndio_report and can turn the volume up, down or set it to silence. A notification of the current level is provided in percentage.

FYI : unmute is not an option.  The mute function will always set the volume to 00%

### Basic Instructions

* Give the sndio_report file permission to execute  
* Assign keyboard shortcuts using the chart below.  
* JWMKit Users can use jwmkit_keys to configure the shortcuts.

### Command Chart

| Action | Command |
|--|--|
| Increase Volume | sndio_report +  |
|  Decrease Volume | sndio_report - |
|  Turn Off  (Mute) | sndio_report m |

**IMPORTANT**: This chart does not use the full path for the file sndio_report.  You may need to change it to the full path.  Example: /home/user/sndio_report

### Easy modifications.
The Defaults may not be optimal for some configuration. Here are some useful tips.

**Notification position**  

Default  :  35 pixels away from the bottom/left edge of the screen.  
Modify  : Change the value of x and y at the top of the script using positive and negative integers. (see chart below) 

|  | Positive number | Negative number |
|--|--|--|
| **x** | Distance from the Left edge  | Distance from the Right edge |
| **y** | Distance from the Top edge  | Distance from the Bottom edge |

 **Mute Message**  
 
Default : 00%  
Modify : search the script for the following

	--text="00%"

Change the 00% to whatever message you want.  Example:

	--text="MUTE"

