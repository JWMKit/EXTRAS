## Tray Toggle

tray_toggle is a bash script that provides the ability to toggle the visibility of a JWM tray.  The script is not an eloquent solution.  It's a hack that was created to fulfill a request.

### How it works

While JWM does provide the ability to auto-hide a tray, it does not provide the ablity to hide/show a tray with a keyboard/mouse shortcut.  When configured properly this script will do the following when executed.

* Detect if the specified tray is part of the JWM configuration.
* If  the tray is not part of the config it will add it.
* if the tray is already part of the config it will remove it.
* Restart JWM so the change is applied. 

### How to use

First you must modify the script for your configuration.

**Step 1** - Configure the variable TRAY.  Change the value of the Variable to the path of the tray you wish to toggle.  
NOTE : The path must be between single quotes as in the example.  

**Step 2** - Select the FILE variable. The FILE variable is declared twice in this file.  One of the lines must be commented out and the selected line must not.  To comment out a line it must start with a #.

Example with the first option selected. (default in the script)

	FILE="$HOME/.config/jwm/jwmrc"   (usually JWM >= 2.4)
	#FILE="$HOME/.jwmrc"             (usually JWM <  2.4)  

**Step 3**  
Now that  you have modified the script you need to link it to a keyboard or mouse shortcut using jwmkit_keys