## Verse of the Day Conky Example

**Purpose**: Demonstrate how to add the verse of the day or a fortune to conky using the verse or fortune command.  This demonstration also illustrates how simple the process is. Anyone can do it.  

**Dependencies**:  conky, verse/forutne/or other command  

**Recommend Usage**: While you could just use the provided configuration, the best usage would be to examine the code and use the gained knowlage in your own config.

**NOTE**:  The preview image shows both the conky and jwm-menu Verse of the Day.

**Tutorial**  
Below is the key part of the provided conky configuration.  More importantly the second line.

	${color grey}Verse of the Day $hr
	
	$color${execi 86400 verse}

The $color commands are only necessary for seting the colors.  You could customize them or simply leave them out like this

	${execi 86400 verse}

Time to cover some questions you may have. Where did you get the number 86400? This is the number of seconds to wait to execute the command again. There is a total of 86400 seconds in 24 hours.  We could set this to a lower number like 3600 to have it run the verse command every hour.

You may also see exec used in conky. So  what is the difference between execi and exec.  exec run the command every time conky updates while execi allows you to specify the update interval in seconds.

Now I'm sure you've already come to the conclusion you could simple replace verse with any other command output text to the terminal.  If so you are correct.  Another common command is fortune.  I'm sure you figured this out.

	${execi 86400 fortune}

So if you want to use the fortune-kjv database saved in ~/jwmkit/ you could do this.

	${execi 86400 fortune ~/jwmkit/fortune-kjv}

