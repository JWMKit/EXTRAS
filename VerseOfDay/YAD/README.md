## Verse of the Day Splash Screen

**Purpose**:  Display a splash screen with the "verse of the day" using output of the verse command.  

**Dependencies**:  verse, yad  

**Recommend Usage**: Add to system start up so the user will see the "verse of the day" after login

**JWM Kit Basic Instructions**

* Give the yad_verse file permission to execute  
* Use JWM Kit Startup to add it to start up.  
