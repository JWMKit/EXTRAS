## Verse Of theDay

**Purpose**  
The following scripts display the output of the verse command line utility in a graphical way.. 

These scripts are to serve as an example and can be modified to use the output of other command line tools.  For example the popular "Fortune" utility that displays random quotes, jokes, words of wisdom, etc. 

**What is "verse"? **  
Verse is a simple commandline utility that provides a daily devotional verse from the KJV Bible. Since verse is a commandline tool it does not provide a graphical window, prompt, etc.  The scripts provided grab the output of the verse command and will display them. Verse is avaliable in the debian repository.

| Directory| Description|
|--|--|
|Conky  | Example of how to add a verse of the day to conky using verse |
| JWM Menu | Create JWM menu with the Verse of the day and configure the menu to pop up when a configured traybutton recieve specific a mouse action.  |
|YAD  | Display a YAD splash screen with the "verse of the day" |
