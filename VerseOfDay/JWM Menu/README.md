## Verse of the Day Menu for JWM

**Purpose**: Create a pop up with the "Verse of the Day" using the output of the verse command when you click on a configured traybutton with the assigned mouse button.  

**Dependencies**:  verse  

**Recommend Usage**:  Configure as right mouse click on the clock.

**NOTE**:  See the image in the conky folder for a preview.

**Files**
jwmkit_verse_mu - the executable file that generates the menu
verse -

