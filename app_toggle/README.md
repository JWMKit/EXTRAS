## app_toggle

Script that provides the ability to toggle the execution status of an application

**Recommended Usage** :  
Assign the script to a traybutton on a JWM tray.  When the traybutton's icon is activiated via mouse click it will either start the assigned app, or close the app if it is already running.

### Instructions  
FYI : These instructions are for users of JWM and JWWMKit, but can be easily modified for other use cases.

**Step 1 - Modify the script** as needed for you personal needs.  Instructions are included in the script. 

 ***Modification 1***  -- Configure the APP variable.   
  The value of this variable should be the command to launch the program you wish to control with the toggle function.  

Example : Assuming the command to launch the desired app is : sys_cal  

Replace this line in the script

	APP="Put the command to run (and kill)"
with the following

	APP="sys_cal"

***Modification 2***  -- Configure the KILL\_SIG variable.     
NOTICE : This step is not required in most cases.  This is a "fix" for cases where the script fails to terminate the application defined with the APP variable.   
  
 
 About the KILL\_SIG variable

* This variable is used to change the kill signal used to terminate the app define with the APP variable
* The default value is empty, and should work in most cases. 
* If default value fails to terminate the application then you can change this value to use a different kill signal.
* The value of this variable must be either empty, or must  start with -s ( or -n for a number) and end with a space. 

The kill signal can be give by name or number.  Start with -s for name and -n for number.  For a list of kill signals  use the command below.  Do you research to learn what they all do.

	kill -l

Example : Assuming you have a command that does not respond to the default value and requires the SIGHUP kill signal.  

Replace the following default line in the script.

	KILL_SIG=""
to this

	KILL_SIG="-s SIGHUP " 

**Step 2 - Configure a traybutton**  
NOTE : The following is a recommended usages.  You may have other ways to make use of the toggle function.  
With the script configured to toggle your desired application it's time to put it to use.

* First I recommend changing the filename of  the script to something that represents it app it is toggling.  For example cal_toggle if you are using it for a calendar. 
* Using jwmkit_trays edit the value of a new or existing traybutton so that a mouse button will activate the script. If necessary assign an icon, or name. Then save.
* Test your new traybutton and enjoy your new toggle action

### Refining with JWM Group Rules  
So it works, but not exactly what you want? Need to set the window to open at specific position, or hide the window's title bar? In JWM you can set group rules to configure all this.  Use jwmkit_groups to to set the values for the app.

Want the group rules to only appy when used with the app\_toggle?  Then just make a symlink to the original file and then use that symlink with app\_toggle and the group rules.

### Resources (additional info)

| Webpage | Description |
|--|--|
|  [JWMKit](https://codeberg.org/JWMKit/JWM_Kit)  | Project Main Page |
| [Sourceforge](https://sourceforge.net/projects/jwmkit/)  | Sourceforge page |
| [JWMKit Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki)  |Wiki Main Page |  
| [JWMKit Trays Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki/trays)  | Wiki for Trays |  
 | [JWMKit Groups  Wiki](https://codeberg.org/JWMKit/JWM_Kit/wiki/groups)  | Wiki for Groups |  
