## X of the Day

YAD script to display an entry for a data file that correlates with the days date.

### Syntax

yad_day < path-to-data-file \> <  title \>

**Example**  

To use the data file verse located in the user's home and to give it the title "Verse of the Day"

```
yad_day $HOME/verses "Verse of the Day"
```

### Data File Structure

- Recommended : One line for each day of the year.  
 366 lines total for a leap year
- Each line starts with a date code, a space, and then the message to display
- Date code is : MM-DD .  Example 01-23 for January 23
- Each entry must be a single line. 
- If you need multiple lines use the new line code \n
- use \t for tab

Data file example

```
01-01 January the First\nLine2\nLine3
01-02 January the Second
01-03 January the thrid
01-04 Ok this is getting old
```

Ok, four days is a good enough example. I'm not doing all 366 days here.

### Provided data files



| Filename | Description | |
|--|--|--|
| quotes | Quotes from well known individuals in tech | Incomplete|
| verses | Verse of the Day from the KJV Bible | Complete 4-18-22|

### More data files needed

If you wish to create data files for this tool please feel free to share them.
