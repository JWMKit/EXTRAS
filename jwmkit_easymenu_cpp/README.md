## JWMKit Easy Menu C++ version

A standalone version of JWMKit Easy Menu written in C++

- Works without JWMKit
- No dependencies
- No need to install anything.
- Easy to compile
- limits apps to a single category.  (prevents multiple listing of an app)

### Compile & Usage
**Step 1**  : Download the file jwmkit_easymenu.cpp

**Step 2**  : Configure you terminal. 

By default xterm is used for terminal apps. As of right now, there is no simple way to change this. You must specify it by altering the code before you compile it.  Close to the top of the file jwmkit_easymenu.cpp you will find a line that defines the terminal.  About line 19 or 20. It looks like this.

	  std::string xterm = "xterm"; 

You can change the "xterm" part to your preferred terminal.  Example:

	  std::string xterm = "xfce4-terminal"; 
  
Save the change and don't change anything else unless you really know what your doing. 
  
**Step 3** :  Compile

Now compile the code to produce an executable. This is simple.  Just run the command below in the same directory you have stored the file " jwmkit_easymenu.cpp"

	g++ -Ofast -o jwmkit_easymenu jwmkit_easymenu.cpp

The -Ofast is for optimizing the compile. You can remove it or add your own if needed.

That's it.  If all went well you should find the executable file "jwmkit_easymenu" in the same directory as the source.

**Step 4** : Ensure that executable file jwmkit_easymenu has permission to run.  From the commandline

	chmod +x jwmkit_easymenu

Now you can test it to see it works by simply entering the command

	jwmkit_easymenu

This should fill the screen with xml text that can be used as a jwm menu.
If it does not work got back to the start and figure out what you did wrong.

**Step 5** : To use Easy menu add it as an include into a rootmenu. 

 Example:
 
```
<JWM>   
   <RootMenu onroot="3"> 
      <Include>exec:jwmkit_easymenu</Include>
   </RootMenu>   
</JWM>
```

Things to remember 

- Replace the exec: line with the full path of where the file is stored.  
- Don't forget the "exec:" part!

### Hyperbola Compatibility.

Requires a version of libc newer than the one provided by Hyperbola.
Sorry :(