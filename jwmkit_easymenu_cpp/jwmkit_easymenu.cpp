#include <string>
#include <iostream>
#include <filesystem>
#include <set>
#include <fstream>
#include <regex>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>

namespace fs = std::filesystem;
std::string regexpress(std::string, std::string);
std::string MakeEntry(std::string, std::string);

int main()
{

  std::string xterm = "xterm"; 
  std::string utility;
  std::string education;
  std::string development;
  std::string game;
  std::string graphics;
  std::string network;
  std::string audiovideo;
  std::string office;
  std::string settings;
  std::string system;
  std::string science;  
  std::string other;
  std::string menu = "<JWM>\n";
  std::string icons[12] = { "applications-utilities", "applications-education", "applications-science", "applications-development",
                            "applications-games", "applications-graphics", "applications-internet", "applications-multimedia",
                            "applications-office", "applications-other", "preferences-system",  "applications-system" };
  
  const char *homedir;
  if ((homedir = getenv("HOME")) == NULL) {
      homedir = getpwuid(getuid())->pw_dir;
  }

  std::string path = "/usr/share/applications/";
  std::string lpath = std::string(homedir).append("/.local/share/applications/");
  std::set<fs::path> file_list;
  
  for (auto &entry : fs::directory_iterator(lpath))
    {
     if (entry.path().extension() == ".desktop")
      file_list.insert(entry.path());
    }
    
  for (auto &entry : fs::directory_iterator(path))
    {
    std::string tpath = lpath + entry.path().filename().c_str();

    if ( (entry.path().extension() == ".desktop")  && (! fs::exists(tpath)) )
      file_list.insert(entry.path());
    }

  for (auto &filename : file_list)
    {
    std::ifstream ifs(filename.c_str());
    std::string content( (std::istreambuf_iterator<char>(ifs) ),
                       (std::istreambuf_iterator<char>()    ) );

    // Ignore hidden items
    std::string Hidden(regexpress("(NoDisplay=true)", content) );
    if (Hidden == "NoDisplay=true")
       continue;

    // create program entries and store in apporpriate category var
    std::string categories(regexpress("Categories=(.+)", content) );
    if (categories.find("Utility") != std::string::npos) 
        utility.append( MakeEntry(content, xterm) );
    else if (categories.find("Development") != std::string::npos)
        development.append( MakeEntry(content, xterm) );
    else if (categories.find("Education") != std::string::npos)
        education.append( MakeEntry(content, xterm) );
    else if (categories.find("Science") != std::string::npos)
        science.append( MakeEntry(content, xterm) );
    else if (categories.find("Game") != std::string::npos)
        game.append( MakeEntry(content, xterm) );
    else if (categories.find("Graphics") != std::string::npos)
        graphics.append( MakeEntry(content, xterm) );
    else if (categories.find("Network") != std::string::npos)
        network.append( MakeEntry(content, xterm) );
    else if (categories.find("AudioVideo") != std::string::npos)
        audiovideo.append( MakeEntry(content, xterm) );
    else if (categories.find("Office") != std::string::npos)
        office.append( MakeEntry(content, xterm) );
    else if (categories.find("Settings") != std::string::npos)
        settings.append( MakeEntry(content, xterm) );
    else if (categories.find("System") != std::string::npos)
        system.append( MakeEntry(content, xterm) );
    else if (categories.find("Security") != std::string::npos)
        system.append( MakeEntry(content, xterm) );
    else
        other.append( MakeEntry(content, xterm) );

    }
 
  // creating a menu entry for each non-empty category var.
  if ( ! utility.empty() )
    menu += "<Menu icon=\"" + icons[0] + "\" label=\"Accessories\">\n" + utility + "</Menu>\n";
  if ( ! education.empty() )
    menu += "<Menu icon=\"" + icons[1] + "\" label=\"Education\">\n" + education + "</Menu>\n";
  if ( ! science.empty() )
    menu += "<Menu icon=\"" + icons[2] + "\" label=\"Science\">\n" + science + "</Menu>\n";
  if ( ! development.empty() )
    menu += "<Menu icon=\"" + icons[3] + "\" label=\"Development\">\n" + development + "</Menu>\n";
  if ( ! game.empty() )
    menu += "<Menu icon=\"" + icons[4] + "\" label=\"Games\">\n" + game + "</Menu>\n";
  if ( ! graphics.empty() )
    menu += "<Menu icon=\"" + icons[5] + "\" label=\"Graphics\">\n" + graphics + "</Menu>\n";
  if ( ! network.empty() )
    menu += "<Menu icon=\"" + icons[6] + "\" label=\"Internet\">\n" + network + "</Menu>\n";
  if ( ! audiovideo.empty() )
    menu += "<Menu icon=\"" + icons[7] + "\" label=\"Multimedia\">\n" + audiovideo + "</Menu>\n";
  if ( ! office.empty() )
    menu += "<Menu icon=\"" + icons[8] + "\" label=\"Office\">\n" + office + "</Menu>\n";
  if ( ! other.empty() )
    menu += "<Menu icon=\"" + icons[9] + "\" label=\"Other\">\n" + other + "</Menu>\n";
  if ( ! settings.empty() )
    menu += "<Menu icon=\"" + icons[10] + "\" label=\"Settings\">\n" + settings + "</Menu>\n";
  if ( ! system.empty() )
    menu += "<Menu icon=\"" + icons[11] + "\" label=\"System\">\n" + system + "</Menu>\n";
  menu += "</JWM>\n";
  std::cout << menu;

  return 0;
    
}

// regex search for items
std::string regexpress(std::string pat, std::string content)
{
  std::regex Pattern (pat);
  std::smatch m;
  std::regex_search(content, m, Pattern);
  return m[1];
}


// create xml program entry
std::string MakeEntry(std::string content, std::string xterm)
{ 
  std::string name(regexpress("Name=(.+)", content) );
  std::string exename(regexpress("Exec=(.+)", content) );
  std::string icon(regexpress("Icon=(.+)", content) );
  std::string terminal(regexpress("(Terminal=true)", content) );
  if (terminal == "Terminal=true")
    return "<Program label=\"" + name + "\" icon=\"" + icon + "\">" + xterm + " -e \"" + exename + "\"</Program>\n";
  else
    return "<Program label=\"" + name + "\" icon=\"" + icon + "\">" + exename + "</Program>\n";
 }   
