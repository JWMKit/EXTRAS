## yad\_fortune - Fortune  Splash Screen

**Purpose**:  Display a splash screen with the with the output of the fortune command.  

**Dependencies**:  fortune, yad  

**Alternative** :   [yad\_fortune\_verse](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/yad_fortune_verse) provides similar function but does not require 'fortune' to be installed, and will automatically find the fortune-kjv database if it is in the same directory

**Recommend Usage**

 -  Add to system start up so the user will see a message at login
 - use with the [fortune-kjv](https://codeberg.org/JWMKit/EXTRAS/src/branch/main/fortune-kjv)  database found in this repository for a bible verse pop up at login.

**Usage / Commandline**  
you can specify a fortune cookie database as an command line argument.  

When run with no argument the splash display the default output of the command "fortune".  

Syntax Examples  :

Use an installed fortune database names "riddles"  

	yad_fortune riddles 

Use the fortune database "fortune-kjv" saved in your home folder.  

	yad_fortune "$HOME/fortune-kjv"

**JWM Kit Basic Instructions**

* Give the yad_fortune file permission to execute  
* Use JWMKit Startup to add it to start up.  