## Gnuinos config

This directory is for suggested improvements for the JWM and JWMKit configuration provided by Gnuinos

### What is Gnuinos

From their [website](https://gnuinos.org/)   

"Gnuinos is a libre spin of Devuan GNU/Linux (a fork of Debian without systemd), allowing users to get control over their computer and ensuring Init Freedom. "

That sounds good to me. Hopefuly as the author of JWMKit my suggestions can help.

### Aditional Issues
Some files in Gnuinos' theme directory (~/.config/jwm/themes) have execute permission. They should not.  I'm sure deleting the files and replacing them with the ones provided here will fix the issue. I advise checking them afterwards

### Usage
How to implement the improvements provided here:  

**Step 1** - Delete the old.  
In order to keep a clean system first delete Gnuinos orginal JWM and JWMKit configurations.  

Files and directories to be deleted.  
NOTE: This including deleting all content in the directories

	~/.jwmrc
	~/.config/jwm
	~/.config/jwmkit
	
**Step 2** - copy in the new

Copy the folders jwm and jwmkit and their included contents to ~/.config/

That's it. Now run jwm -restart to see the changes.

Reminder : If your creating a build (ISO) of Gnuinos you need to update /etc/skell or wherever you put configurations to be be used when creating new users.

## Change log
NOTE: This is from memory so it's probably not complete.

**Renamed Themes.** 

 - Theme files must end with either theme or jwmrc to be compatible with jwmkit_appearances.
 
**file: ~/.jwmrc  --> ~/.config/jwmrc**

- move ~/.jwmrc to ~/.config/jwm/jwmrc. It's cleaner and simpler to be located with the rest of the jwm config. 
- xml syntax - ampersand should be escaped.
- startup commands should not be in the .jwmrc file. Moved these lines to ~/.config/jwm/startup
- Changed the Include line for the theme from the acual theme to the file  used by JWMKit to configure the theme. (this also needs configured in the ~/.config/jwmkit/settings)
 
**file: ~/.config/jwm/startup**

- added startcommands that were in ~/.jwmrc (as stated above)

**file: ~/.config/jwm/icons**

- added a path for some deepsea and hicolor icons

**file: ~/.config/jwm/menu-firefox**

- changed firefox to icecat

**file: ~/.config/jwm/menu-spacefm**

- removed non-working root window launcher

**file: ~/.config/jwm/menu-terminal**

- fix sakura incompatibility by simplifying the commands

**file: ~/.config/jwm/tray-bottom**

- changed firefox traybutton to icecat
- changed the clock action from the missing yad_pop_cal command to jwmkit_calendar
- change "invisible" jwmkit_logout button to gninos exit prompt. right click will runs jwm -restart.
- removed un-used "invisible" launchers

**file: ~/.config/jwm/tray-top**

- changed firefox traybutton to icecat
- changed htop to use x-terminal-emulator (sakura) instead of xterm

**file: ~/.config/jwmkit/menu-jwmkit**

- change launcher for the retired jwmkit_wallpaper to new jwmkit_desktops

**file: ~/.config/jwmkit/settings**

- change the theme= line to point to the correct file  
This should not point to an acual theme in use, but the to file jwmkit_appearance will use to configure the theme
- removed unnecessary lines ( I don't even remember)

**file: ~/.config/jwmkit/setting_su**

add time=sudop.  This will allow users to give jwmkit_time permission to set system time.

**file ~/.config/jwmkit/restore/original.restore (new file)**

- jwmkit restore point containing this configuration. Using the name "original.restore" instead of the date tells jwmkit to ignore the restore point packaged with jwmkit and found in /etc/jwmkit/


