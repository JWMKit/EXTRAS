## JWM Kit Easy Menu

JWM Kit Easy Menu without JWM Kit.

This is a stand alone version of JWM Kit Easy Menu for users who wishes to manually create their own jwm configuration without the tools provided in JWM Kit, but still wish to use JWM Kit's Easy Menu to provide an automatic menu.


To use Easy menu add it as an include into a rootmenu.  Example:

```
<JWM>   
   <RootMenu onroot="3"> 
      <Include>exec:jwmkit_easymenu</Include>
   </RootMenu>   
</JWM>
```

***

### Dependencies

- python3

That's it.  Short list. :)

***

### Features

- Hide apps
- Hide entire categories
- Specify a Language and/or Territory
- Specify a Terminal to use with terminal apps
- Rename a category
- Merge categories
- Prevent Duplicate entries for an app   
(for apps that appear in more than one categories)


### Syntax
To impliment all the features listed above and more see the syntax.txt file to learn how to craft your own custom command.
